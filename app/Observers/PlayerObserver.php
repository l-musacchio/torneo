<?php

namespace App\Observers;

use App\Models\Player;
use App\Models\Team;

class PlayerObserver
{
    /**
     * Handle the Player "created" event.
     *
     * @param  \App\Models\Player  $player
     * @return void
     */
    public function created(Player $player)
    {
        //
    }

    /**
     * Handle the Player "updated" event.
     *
     * @param  \App\Models\Player  $player
     * @return void
     */
    public function updated(Player $player)
    {
        /*$team = Team::find($player->team_id);
        $kills = $team->kills + $player->kills;
        $deaths = $team->deaths + $player->deaths;
        $team->update([
            'kills' => $kills,
            'deaths' => $deaths
        ]); */
    }

    /**
     * Handle the Player "deleted" event.
     *
     * @param  \App\Models\Player  $player
     * @return void
     */
    public function deleted(Player $player)
    {
        //
    }

    /**
     * Handle the Player "restored" event.
     *
     * @param  \App\Models\Player  $player
     * @return void
     */
    public function restored(Player $player)
    {
        //
    }

    /**
     * Handle the Player "force deleted" event.
     *
     * @param  \App\Models\Player  $player
     * @return void
     */
    public function forceDeleted(Player $player)
    {
        //
    }
}
