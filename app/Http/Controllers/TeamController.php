<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Team;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teams = Team::all();
        $vac = compact('teams');
        return view('admin.team.index',$vac);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.team.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validator($request);
        $games_played = $request['win'] + $request['lose'];
        Team::create([
            'name' => $request->input('name'),
            'tag' => $request->input('tag'),
            'games_played' => $games_played,
            'win' => $request->input('win'),
            'lose' => $request->input('lose'),
            'kills' => 0,
            'deaths' => 0
        ]);

        return redirect()->route('team.index')->with('notice', 'El equipo '.ucfirst($request['name']).' se agrego ANAAAAAANAAANASHEEEEEEE');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $team = Team::find($id);
        $vac = compact('team');
        return view('team.show',$vac);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $team = Team::find($id);
        $vac = compact('team');
        return view('admin.team.edit',$vac);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $team = Team::find($id);
        $games_played = $request['win'] + $request['lose'];
        $team->update([
            'name' => $request['name'],
            'tag' => $request['tag'],
            'games_played' => $games_played,
            'win' => $request['win'],
            'lose' => $request['lose'],
            'kills' => $request['kills'],
            'deaths' => $request['deaths']
        ]);
        return redirect()->route('team.index')->with('notice', 'El equipo '.ucfirst($team->name).' se edito de una.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\
     */
    
    public function delete($id)
    {
        $team = Team::find($id);
        $team->delete();
        return redirect()->route('team.index')->with('notice', 'El equipo '.Ucfirst($team->name).' se fue DELETEATRES BROOO');
    }
    
    public function validator(Request $request)
    {
        $rules = [
            'name' => 'required|unique:teams|string|max:50',
            'tag' => 'required|unique:teams|string|max:4',
            'win' => 'required|integer',
            'lose' => 'required|integer',
        ];
        $message = [
            'required' => 'El campo es obligatorio.',
            'unique' => 'El equipo ya existe.',
            'string' => 'Solo se admiten letras.',
            'integer' => 'Solo se admiten numeros.',
        ];
        return $this->validate($request, $rules, $message);
    }
}