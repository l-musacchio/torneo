<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Player;
use App\Models\Team;

class PlayerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $players = Player::all();
        $vac = compact('players');
        return view('admin.player.index',$vac);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $teams = Team::all();
        $vac = compact('teams');
        return view('admin.player.create',$vac);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validator($request);
        $KDA = $this->calcularKDA($request['kills'], $request['deaths'],$request['assists']);
        $player = Player::create([
            'name' => $request['name'],
            'kills' => $request['kills'],
            'deaths' => $request['deaths'],
            'assists' => $request['assists'],
            'damage' => $request['damage'],
            'KDA' => $KDA,   
            'kill_percentage' => $request['kill_percentage'],
            'team_id' => $request['team']
        ]);
        
        return redirect()->route('player.index')->with('notice', 'El jugador '. ucfirst($request['name']) .' se agrego INSSSSSSSSSSSSSSSTA');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $player = Player::find($id);
        $teams = Team::all();
        $vac = compact('player','teams');
        return view('admin.player.edit',$vac);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $player = Player::find($id);
        $kills = $player->kills + $request['kills'];
        $deaths = $player->deaths + $request['deaths'];
        $assists = $player->assists + $request['assists'];
        $damage = $player->damage + $request['damage'];
        $KDA = $this->calcularKDA($kills, $deaths,$assists);
        if ($player->kill_percentage > 0) {
            $KP = ($player->kill_percentage + $request['kill_percentage'])/2;
        }else {
            $KP = $request['kill_percentage'];
        };
        $player->update([
            'name' => $request->input('name'),
            'kills' => $kills,
            'deaths' => $deaths,
            'assists' => $assists,
            'damage' => $damage,
            'KDA' => $KDA,
            'kill_percentage' => $KP,
            'team_id' => $request->input('team')
        ]);

        return redirect()->route('player.index')->with('notice', 'El jugador '.ucfirst($player->name).' se edito de una.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $player = Player::find($id);
        $player->delete();
        return redirect()->route('player.index')->with('notice','El jugador '.ucfirst($player->name).' se elimino porque era malardo');
    }

    public function validator(Request $request)
    {
        $rules = [
            'name' => 'required|unique:players|string|max:50',
            'kills' => 'required|integer',
            'deaths' => 'required|integer',
            'assists' => 'required|integer',
            'damage' => 'required',
        ];
        $message = [
            'max' => "Tiene que ser menor a 50",
            'required' => 'El campo es obligatorio.',
            'unique' => 'El jugador ya existe.',
            'string' => 'Solo se admiten letras.',
            'integer' => 'Solo se admiten numeros.',
        ];
        return $this->validate($request, $rules, $message);
    }

    public function calcularKDA($k,$d,$a)
    {
        return $kda = ($d==0) ? $k + $a : ($k + $a) / $d ; 
    }
}

