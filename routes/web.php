<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\TeamController;
use App\Http\Controllers\PlayerController;
use App\Models\Team;
use App\Models\Player;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::resources([
    'team' => TeamController::class,
    'player' => PlayerController::class
]);

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::get('/players',function(){
    return view('players');
});

Route::group(['middleware' => ['auth']], function () {
    //! Rutas del administrador
    Route::get('/admin',function(){return view('admin.index');})->name('admin');

    Route::get('/team/{id}/delete', [TeamController::class ,'delete'])->name('team.delete');
    Route::get('/player/{id}/delete', [PlayerController::class ,'delete'])->name('player.delete');
});

//! Variables globales
View::composer('index', function ($view) {
    $teams = DB::table('teams')->orderBy('win','desc')->get();
    $view->with('teams', $teams);
});
View::composer('players', function ($view) {
    $players = Player::all();
    $view->with('players', $players);
});
