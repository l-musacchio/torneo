@extends('layouts.app')
@section('content')
<div class="container">
    <h1 class="text-center mt-5 mb-2 pt-4" style="font-family: Bebas Neue; font-size: 70px;letter-spacing: 10px;color:#000">Torneo de Amigos</h1>
    <h3 class="text-center my-4" style="font-family: Bebas Neue; font-size: 70px;color:#000">Ranking de Jugadores</h3>
    <table style="background-color: rgba(0,0,0,0.85);border-radius:20px" class="table table-dark table-sortable table-borderless text-center table-hover">
        <thead>
            <tr style="background-color: rgba(0, 0, 0, 0.9)" class="text-center">
                <th scope="col">Name</th>
                <th scope="col">Kills</th>
                <th scope="col">Deaths</th>
                <th scope="col">Assists</th>
                <th scope="col">Damage</th>
                <th scope="col">KDA</th>
                <th scope="col">KP</th>
                <th scope="col">Team</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($players as $player)
                <tr>
                    <td style="font-weight:bold !important"><a class="text-reset" href="https://las.op.gg/summoner/userName={{$player->name}}">{{Ucfirst($player->name)}}</a></td>
                    <td style="color:rgb(255, 102, 0); font-weight:bold !important">{{$player->kills}}</td>
                    <td style="color: rgb(255, 0, 0); font-weight:bold !important">{{$player->deaths}}</td>
                    <td style="color: rgb(0, 238, 255); font-weight:bold !important">{{$player->assists}}</td>
                    <td style="color: rgb(212, 0, 232); font-weight:bold !important">{{$player->damage}}</td>
                    @if($player->KDA >= 5)
                        <td style="color: rgb(255, 174, 0); font-weight:bold !important">{{$player->KDA}}</td>
                    @elseif($player->KDA >= 4)
                        <td style="color:#1f8ecd; font-weight:bold !important">{{$player->KDA}}</td>
                    @elseif($player->KDA >= 3)
                        <td style="color: #2daf7f; font-weight:bold !important">{{$player->KDA}}</td>
                    @elseif($player->KDA > 2)
                        <td style="font-weight:bold">{{$player->KDA}}</td>
                    @elseif($player->KDA < 2)
                        <td style="color: #ff0000; font-weight:bold !important">{{$player->KDA}}</td>
                    @else
                        <td style="font-weight:bold">{{$player->KDA}}</td>
                    @endif
                    <td style="font-weight:bold">{{$player->kill_percentage}}</td>
                    <td style="font-weight:bold">
                        @if ($player->team)
                            {{Ucfirst($player->team->name)}}
                        @endif
                    </td>
                </tr>
            @endforeach    
        </tbody>
      </table>
    <div class="d-flex justify-content-center">
        <button style="background-color: #000" class="col-2 btn rounded-pill"><a href="{{'/'}}" style="display: block;text-decoration: none; color:#fff">Volver</a></button>
    </div>
</div>
@endsection