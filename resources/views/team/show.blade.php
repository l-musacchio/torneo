@extends('layouts.app')
@section('content')
<div class="container">
    <h1 class="text-center mt-5 mb-2 pt-4" style="font-family: Bebas Neue; font-size: 70px;letter-spacing: 10px;color:#000">Torneo de Amigos</h1>
    <h3 class="text-center my-4" style="font-family: Bebas Neue; font-size: 70px;color:#000">Jugadores de {{Ucfirst($team->name)}} ({{strtoupper($team->tag)}})</h3>
    <table style="background-color: rgba(0,0,0,0.850)" class="table table-dark text-center table-hover">
        <thead>
            <tr style="background-color: rgba(0, 0, 0, 0.9)" class="text-center">
                <th scope="col">Name</th>
                <th scope="col">Kills</th>
                <th scope="col">Deaths</th>
                <th scope="col">Assists</th>
                <th scope="col">Damage</th>
                <th scope="col">KDA</th>
                <th scope="col">KP</th>
                <th scope="col">Team</th>
            </tr>
        </thead>
        <tbody>
            @if (isset($team))
                @foreach ($team->players as $player)
                    <tr>
                        <td style="font-weight:bold !important"><a class="text-reset" href="https://las.op.gg/summoner/userName={{$player->name}}">{{Ucfirst($player->name)}}</a></td>
                        <th style="color:rgb(255, 102, 0)">{{$player->kills}}</th>
                        <th style="color: rgb(255, 0, 0)">{{$player->deaths}}</th>
                        <th style="color: rgb(0, 238, 255)">{{$player->assists}}</th>
                        <th style="color: rgb(212, 0, 232)">{{$player->damage}}</th>
                        @if($player->KDA >= 5)
                            <th style="color: rgb(255, 174, 0)">{{$player->KDA}}</th>
                        @elseif($player->KDA >= 4)
                            <th style="color:#1f8ecd">{{$player->KDA}}</th>
                        @elseif($player->KDA >= 3)
                            <th style="color: #2daf7f">{{$player->KDA}}</th>
                        @elseif($player->KDA > 2)
                            <th>{{$player->KDA}}</th>
                        @elseif($player->KDA < 2)
                            <th style="color: #ff0000">{{$player->KDA}}</th>
                        @else
                            <th>{{$player->KDA}}</th>
                        @endif
                        <th>%{{$player->kill_percentage}}</th>
                        <th>
                            @if ($player->team)
                                {{Ucfirst($player->team->name)}}
                            @endif
                        </th>
                    </tr>
                @endforeach    
            @endif
        </tbody>
      </table>
    <div class="container text-center">
        <button style="background-color: #000" class="col-2 btn rounded-pill"><a href="{{'/'}}" style="display: block;text-decoration: none; color:#fff">Volver</a></button>
    </div>
</div>
@endsection