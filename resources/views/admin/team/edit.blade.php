@extends('layouts.guest')
@section('content')
<div class="col-12">
    <div class="col-12 text-center mb-5">
        <h2>Editar Team</h2>
    </div>
    <form class="col-12 mx-auto" action="{{route('team.update',['team'=>$team])}}" method="post" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div class="form-row justify-content-center">
            <div class="form-group col-12 col-md-6 col-lg-2">
                <label class="h4 pb-2" for="">Nombre</label>
                <input name="name" class="form-control" type="text" value="{{ $team->name }}">
                <small class="form-text text-muted"><b>Nuevo nombre del equipo</b></small>
                @error('name')
                <div id="error" class="alert alert-danger mx-auto col-11 col-sm-4 col-lg-12"><span>{{ $message }}</span></div>
                @enderror
            </div>
            <div class="form-group col-12 col-md-6 col-lg-1">
                <h4 class="text-center"><label for="">Tag</label></h4>
                <input name="tag" class="form-control" type="text" value="{{ $team->tag }}">
                <small class="form-text text-muted"><b>Tag del equipo</b></small>
                @error('tag')
                <div id="error" class="alert alert-danger mx-auto col-11 col-sm-4 col-lg-12"><span>{{ $message }}</span></div>
                @enderror
            </div>
            <div class="form-group col-12 col-md-6 col-lg-1">
                <h4 class="text-center"><label for="">PG</label></h4>
                <input name="win" class="form-control" type="number" value="{{ $team->win}}">
                <small class="form-text text-muted"><b>Partidos ganados</b></small>
                @error('win')
                <div id="error" class="alert alert-danger mx-auto col-11 col-sm-4 col-lg-12"><span>{{ $message }}</span></div>
                @enderror
            </div>
            <div class="form-group col-12 col-md-6 col-lg-1">
                <h4 class="text-center"><label for="">PP</label></h4>
                <input name="lose" class="form-control" type="number" value="{{ $team->lose }}">
                <small class="form-text text-muted"><b>Partidos perdidos</b></small>
                @error('lose')
                <div id="error" class="alert alert-danger mx-auto col-11 col-sm-4 col-lg-12"><span>{{ $message }}</span></div>
                @enderror
            </div>
            <div class="form-group col-12 col-md-6 col-lg-1">
                <h4 class="text-center"><label for="">Kills</label></h4>
                <input name="kills" class="form-control" type="number" value="{{ $team->kills }}">
                <small class="form-text text-muted"><b>Kills</b></small>
                @error('kills')
                <div id="error" class="alert alert-danger mx-auto col-11 col-sm-4 col-lg-12"><span>{{ $message }}</span></div>
                @enderror
            </div>
            <div class="form-group col-12 col-md-6 col-lg-1">
                <h4 class="text-center"><label for="">Deaths</label></h4>
                <input name="deaths" class="form-control" type="number" value="{{ $team->deaths }}">
                <small class="form-text text-muted"><b>Deaths</b></small>
                @error('deaths')
                <div id="error" class="alert alert-danger mx-auto col-11 col-sm-4 col-lg-12"><span>{{ $message }}</span></div>
                @enderror
            </div>
            <div id="buttons" class="text-center mt-3 col-12">
                <button class="mx-2 btn btn-outline-dark rounded-pill" type="submit">Editar</button>
                <a href="{{route('team.index')}}"><button class="btn btn-outline-dark rounded-pill" type="button">Cancelar</button></a>
            </div>
        </div>
    </form>
</div>
@endsection