@extends('layouts.guest')
@section('content')
<div class="container">
    <div class="col-12">
        <div class="mx-auto text-center col-12">
            <h2>Teams</h2>
            <a href="{{route('admin')}}" style="text-decoration: none; color:black"><i class="fas fa-arrow-circle-left"></i> Volver</a>
        </div>
        <div class="row justify-content-center mx-auto mb-3 col-12 col-md-8 col-lg-6 col-xl-4">
            <div class="my-3 col-6 col-md-4">
                <a href="{{route('team.create')}}"><button class="btn btn-outline-dark rounded-pill w-100">Nuevo</button></a>
            </div>
        </div>
        <div class="my-2 col-12 text-center">
            @if(Session::has('notice'))
            <h3 class="my-auto text-success"><strong>{{ Session::get('notice') }}</strong></h3>
            @endif
        </div>
    </div>
    <div class="table table-hover" style="overflow-x:auto;">
        <table class="mx-auto">
            <thead>
                <tr class="text-center">
                    <th scope="col">#</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Tag</th>
                    <th scope="col">PJ</th>
                    <th scope="col">PG</th>
                    <th scope="col">PP</th>
                    <th scope="col">Kills</th>
                    <th scope="col">Deaths</th>
                    <th scope="col" colspan="2">Opciones</th>
                </tr>
            </thead>
            <tbody>
                @if (isset($teams))
                    @foreach ($teams as $team)
                    <tr>
                        <th>{{$team->id}}</th>
                        <th>{{Ucfirst($team->name)}}</th>
                        <th>{{strtoupper($team->tag)}}</th>
                        <th>{{$team->games_played}}</th>
                        <th>{{$team->win}}</th>
                        <th>{{$team->lose}}</th>
                        <th>{{$team->kills}}</th>
                        <th>{{$team->deaths}}</th>
                        <th><a href="{{ route('team.edit',['team'=>$team])}}"><span class="material-icons" title="Editar">edit</span></a></th>
                        <th><a href="{{ route('team.delete',['id'=>$team->id])}}"><span class="material-icons" title="Eliminar">delete_outline</span></a></th>
                    </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
</div>
@endsection