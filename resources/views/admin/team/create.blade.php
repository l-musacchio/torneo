@extends('layouts.guest')
@section('content')
<div class="col-12">
    <div class="col-12 text-center mb-5">
        <h2>Nuevo Team</h2>
    </div>
    <form class="col-12 mx-auto" action="{{route('team.store')}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-row justify-content-center">
            <div class="form-group col-12 col-md-6 col-lg-2">
                <h4><label for="">Nombre</label></h4>
                <input name="name" class="form-control" type="text" value="{{ old('name') }}">
                <small class="form-text text-muted"><b>Nombre del nuevo equipo</b></small>
                @error('name')
                <div id="error" class="alert alert-danger mx-auto col-11 col-sm-4 col-lg-12"><span>{{ $message }}</span></div>
                @enderror
            </div>
            <div class="form-group col-12 col-md-6 col-lg-1">
                <h4 class="text-center"><label for="">Tag</label></h4>
                <input name="tag" class="form-control" type="text" value="{{ old('tag') }}">
                <small class="form-text text-muted"><b>Tag del equipo</b></small>
                @error('tag')
                <div id="error" class="alert alert-danger mx-auto col-11 col-sm-4 col-lg-12"><span>{{ $message }}</span></div>
                @enderror
            </div>
            <div class="form-group col-12 col-md-6 col-lg-1">
                <h4 class="text-center"><label for="">PG</label></h4>
                <input name="win" class="form-control" type="number" value="{{ old('win') }}">
                <small class="form-text text-muted"><b>Partidos ganados</b></small>
                @error('win')
                <div id="error" class="alert alert-danger mx-auto col-11 col-sm-4 col-lg-12"><span>{{ $message }}</span></div>
                @enderror
            </div>
            <div class="form-group col-12 col-md-6 col-lg-1">
                <h4 class="text-center"><label for="">PP</label></h4>
                <input name="lose" class="form-control" type="number" value="{{ old('lose') }}">
                <small class="form-text text-muted"><b>Partidos perdidos</b></small>
                @error('lose')
                <div id="error" class="alert alert-danger mx-auto col-11 col-sm-4 col-lg-12"><span>{{ $message }}</span></div>
                @enderror
            </div>
        <div id="buttons" class="text-center mt-3 col-12">
            <button class="mx-2 btn btn-outline-dark rounded-pill" type="submit">Agregar</button>
            <a href="{{route('team.index')}}"><button class="btn btn-outline-dark rounded-pill" type="button">Cancelar</button></a>
        </div>
    </form>
</div>
@endsection