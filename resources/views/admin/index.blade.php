@extends('layouts.guest')
@section('content')
<div class="container">
    <div id="div-admin-buttons" class="row mt-3 justify-content-center">
        <div class="col-6 col-md-3">
            <a href="{{route('player.index')}}">
                <button class="btn col-12 btn-outline-dark rounded-pill">PLAYERS NEFASTOS</button>
            </a>
        </div>
        <div class="col-6 col-md-3">
            <a href="{{route('team.index')}}">
                <button class="btn col-12 btn-outline-dark rounded-pill">TEAMS</button>
            </a>
        </div>
    </div>
</div>
@endsection