@extends('layouts.guest')
@section('content')
<div class="container">
    <div class="col-12">
        <div class="mx-auto text-center col-12">
            <h2>Players</h2>
            <a href="{{route('admin')}}" style="text-decoration: none; color:black"><i class="fas fa-arrow-circle-left"></i> Volver</a>
        </div>
        <div class="row justify-content-center mx-auto mb-3 col-12 col-md-8 col-lg-6 col-xl-4">
            <div class="my-3 col-6 col-md-4">
                <a href="{{route('player.create')}}"><button class="btn btn-outline-dark rounded-pill w-100">Nuevo</button></a>
            </div>
        </div>
        <div class="my-2 col-12 text-center">
            @if(Session::has('notice'))
            <h3 class="my-auto text-success"><strong>{{ Session::get('notice') }}</strong></h3>
            @endif
        </div>
    </div>
    <div class="table table-hover" style="overflow-x:auto;">
        <table class="mx-auto">
            <thead>
                <tr class="text-center">
                    <th scope="col">#</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Kills</th>
                    <th scope="col">Deaths</th>
                    <th scope="col">Assists</th>
                    <th scope="col">Damage</th>
                    <th scope="col">KDA</th>
                    <th scope="col">KP</th>
                    <th scope="col">Team</th>
                    <th scope="col" colspan="2">Opciones</th>
                </tr>
            </thead>
            <tbody>
                @if (isset($players))
                    @foreach ($players as $player)
                    <tr>
                        <th>{{$player->id}}</th>
                        <th>{{$player->name}}</th>
                        <th>{{$player->kills}}</th>
                        <th>{{$player->deaths}}</th>
                        <th>{{$player->assists}}</th>
                        <th>{{$player->damage}}</th>
                        <th>{{$player->KDA}}</th>
                        <th>%{{$player->kill_percentage}}</th>
                        <th>
                            @if ($player->team)
                                {{Ucfirst($player->team->name)}}
                            @endif
                        </th>
                        <th><a href="{{ route('player.edit',['player'=>$player])}}"><span class="material-icons" title="Editar">create</span></a></th>
                        <th><a href="{{ route('player.delete',['id'=>$player->id])}}"><span class="material-icons" title="Eliminar">delete</span></a></th>
                    </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
</div>
@endsection