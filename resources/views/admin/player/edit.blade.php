@extends('layouts.guest')
@section('content')
<div class="col-12">
    <div class="col-12 text-center mb-5">
        <h2>Editar Player</h2>
    </div>
    <form class="col-12 mx-auto" action="{{route('player.update',['player'=>$player])}}" method="post" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div class="form-row justify-content-center">
            <div class="form-group col-12 col-md-6 col-lg-2">
                <h4><label for="">Nombre</label></h4>
                <input name="name" class="form-control" type="text" value="{{ $player->name }}">
                <small class="form-text text-muted"><b>Nombre del jugador</b></small>
                @error('name')
                <div id="error" class="alert alert-danger mx-auto col-11 col-sm-4 col-lg-12"><span>{{ $message }}</span></div>
                @enderror
            </div>
            <div class="form-group col-12 col-md-6 col-lg-1">
                <h4 class="text-center"><label for="">Kills</label></h4>
                <input name="kills" class="form-control" type="text" value="{{ old('kills') }}">
                <small class="form-text text-muted"><b>Kills acumuladas</b></small>
                @error('kills')
                <div id="error" class="alert alert-danger mx-auto col-11 col-sm-4 col-lg-12"><span>{{ $message }}</span></div>
                @enderror
            </div>
            <div class="form-group col-12 col-md-6 col-lg-1">
                <h4 class="text-center"><label for="">Deaths</label></h4>
                <input name="deaths" class="form-control" type="number" value="{{ old('deaths') }}">
                <small class="form-text text-muted"><b>Muertes acumuladas</b></small>
                @error('deaths')
                <div id="error" class="alert alert-danger mx-auto col-11 col-sm-4 col-lg-12"><span>{{ $message }}</span></div>
                @enderror
            </div>
            <div class="form-group col-12 col-md-6 col-lg-1">
                <h4 class="text-center"><label for="">Assists</label></h4>
                <input name="assists" class="form-control" type="number" value="{{ old('assists') }}">
                <small class="form-text text-muted"><b>Asistencias acumuladas</b></small>
                @error('assists')
                <div id="error" class="alert alert-danger mx-auto col-11 col-sm-4 col-lg-12"><span>{{ $message }}</span></div>
                @enderror
            </div>
            <div class="form-group col-12 col-md-6 col-lg-1">
                <h4 class="text-center"><label for="">Damage</label></h4>
                <input name="damage" class="form-control" type="number" value="{{ old('damage') }}">
                <small class="form-text text-muted"><b>Daño acumulado</b></small>
                @error('damage')
                <div id="error" class="alert alert-danger mx-auto col-11 col-sm-4 col-lg-12"><span>{{ $message }}</span></div>
                @enderror
            </div>
            <div class="form-group col-12 col-md-6 col-lg-1">
                <h4 class="text-center"><label for="">KP</label></h4>
                <input name="kill_percentage" class="form-control" type="number" min="0" step=".01" value="{{ old('kill_percentage') }}">
                <small class="form-text text-muted"><b>KP</b></small>
                @error('kill_percentage')
                <div id="error" class="alert alert-danger mx-auto col-11 col-sm-4 col-lg-12"><span>{{ $message }}</span></div>
                @enderror
            </div>
            <div class="form-group col-12 col-md-6 col-lg-2">
                <label class="h4 pb-2" for="">Equipo</label>
                <select class="form-control mb-1" name="team">
                    <option value="">Elegi un team</option>
                    @if(isset($teams))
                        @foreach($teams as $team)
                            <option @if ($team->id == $player->team_id) {{"selected"}}@endif value="{{$team->id}}">{{Ucfirst($team->name)}}</option>
                        @endforeach
                    @endif
                </select>
                @error('team')
                <div id="error" class="alert alert-danger mx-auto col-11 col-sm-4 col-lg-12"><span>{{ $message }}</span></div>
                @enderror
            </div>
            <div id="buttons" class="text-center mt-3 col-12">
                <button class="mx-2 btn btn-outline-dark rounded-pill" type="submit">Agregar</button>
                <a href="{{route('player.index')}}"><button class="btn btn-outline-dark rounded-pill" type="button">Cancelar</button></a>
            </div>
        </div>
    </form>
</div>
@endsection